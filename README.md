# 安全教育平台API文档

# 登录 GET

https://省子域名.xueanquan.com/LoginHandler.ashx?&userName=用户名&password=密码&checkcode=验证码&type=login&loginType=1&r=随机数（Math.random()）&_=13位时间戳

# 获取验证码 GET

https://省子域名.xueanquan.com/checkcode.aspx?codetype=1&r=随机数（Math.random()）

# 获取账号信息 GET

api1：https://省子域名.xueanquan.com/LoginHandler.ashx?r=随机数（Math.random()）&loginType=1&_=13位时间戳

api2：https://huodongapi.xueanquan.com/p/省份拼音/Topic/topic/platformapi/api/v1/users/user-info

# 班级信息 GET

https://省子域名.xueanquan.com/CommonHandler/SiteIndex.ashx?method=regionpaiming&scoretype=班级类型（1-4）

# 获取第三方登录url GET

https://省子域名.xueanquan.com/CommonHandler/SiteIndex.ashx?method=WebInfoGet&Query=

# 获取作业信息 GET

https://省子域名.xueanquan.com/JiaTing/CommonHandler/MyHomeWork.ashx?method=作业类型（WebInfoGet，showaddialog，myhomeworkinfo，studylist，videowatch，insertvideozan，insertpsyzan，psystudylist）

# 获取作业页面地址

https://chengdu.xueanquan.com/webapi/jt/MyHomeWork.html?grade=9&classroom=班级id&cityid=城市id&r=随机数&host=1省子域名.xueanquan.com

# 活动视频签到 POST

https://huodongapi.xueanquan.com/p/sichuan/Topic/topic/platformapi/api/v1/records/sign

参数 {"specialId":待研究（活动标识？）,"step":活动步骤}

# 活动问卷提交 POST

https://huodongapi.xueanquan.com/Topic/topic/main/api/v1/records/survey

参数 太长了自己抓去



时间：2024
specialId可以从活动页面的body的data-specialid中获取
省份拼音可能有时候是城市拼音
新登录API：https://appapi.xueanquan.com/usercenter/api/v3/wx/login
需要使用JSON：是
    JSON格式：
             {
 				 "username": "$用户账户$",
 				 "password": "$用户密码$",
				  "loginOrigin": $登录类型 （0：教师   1：学生）$
			}
            使用时$$和里面的内容更改，""不要去掉