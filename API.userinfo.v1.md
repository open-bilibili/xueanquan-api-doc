## 获取用户信息 (GET)

#### 调用地址

https://huodongapi.xueanquan.com/p/{参数一}/Topic/topic/platformapi/api/v1/users/user-info

#### 参数

|字段|必选|类型|说明|
|----|----|----|----|
|{参数一}|true|string|省份拼音|

#### 返回

|返回值字段|字段类型|字段说明|
|----------|--------|--------|
